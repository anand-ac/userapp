//
//  MenuViewController.swift
//  UserApp
//
//  Created by Kumar, Ananda on 26/12/21.
//

import UIKit

class MenuViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func form_Click(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "form") as? FormViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func news_Click(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "news") as? NewsViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    

}
