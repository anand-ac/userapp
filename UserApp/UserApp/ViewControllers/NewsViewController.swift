//
//  NewsViewController.swift
//  UserApp
//
//  Created by Kumar, Ananda on 26/12/21.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

class NewsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableNews: UITableView!
    let refreshControl = UIRefreshControl()
    
    var arrNews = [NSDictionary]()
    var fact = ""
    var imageurl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tableNews.addSubview(refreshControl)
        COMMON.showProgressbar(vc: self, msg: "Downloading...")
        perform(#selector(getNewsData), with: nil, afterDelay: 0.1)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        getNewsData()
    }
    
    @objc func getNewsData()
    {
        APICall.performGetAPICall(apiName: IMAGEURL, obj: self) {(jsonResult) in
            DispatchQueue.main.async { [self] in
                if(jsonResult!.count > 0)
                {
                    
                    if (jsonResult?.value(forKey: "message") != nil)
                    {
                        imageurl = jsonResult?.value(forKey: "message") as! String
                        calltoFacts()
                        
                    }
                    
                }
            }
        }
        
    }
    
    func calltoFacts()
    {
        APICall.performGetAPICall(apiName: FACTSURL, obj: self) {(jsonResult) in
            DispatchQueue.main.async { [self] in
                if(jsonResult!.count > 0)
                {
                    if (jsonResult?.value(forKey: "fact") != nil)
                    {
                        fact = jsonResult?.value(forKey: "fact") as! String
                        let dict:NSDictionary = ["imageurl" : imageurl, "fact" : fact]
                        arrNews.append(dict)
                        tableNews.reloadData()
                        COMMON.hideProgressbar()
                        refreshControl.endRefreshing()
                    }
                    
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsTableViewCell", for: indexPath) as! NewsTableViewCell
        let dict = arrNews[indexPath.row]
        let imageurl = dict.value(forKey: "imageurl") as! String
        cell.lblNews.text = dict.value(forKey: "fact") as? String
        cell.imgNews.loadThumbnail(urlSting: imageurl)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
}

extension UIImageView {
    
    func loadThumbnail(urlSting: String) {
        guard let url = URL(string: urlSting) else { return }
        image = nil
        
        if let imageFromCache = imageCache.object(forKey: urlSting as AnyObject) {
            image = imageFromCache as? UIImage
            return
        }
        APICall.downloadImage(url: url) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let data):
                guard let imageToCache = UIImage(data: data) else { return }
                imageCache.setObject(imageToCache, forKey: urlSting as AnyObject)
                self.image = UIImage(data: data)
            case .failure(_):
                self.image = UIImage(named: "noImage")
            }
        }
    }
}

