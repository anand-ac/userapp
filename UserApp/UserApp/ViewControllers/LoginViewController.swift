//
//  ViewController.swift
//  UserApp
//
//  Created by Kumar, Ananda on 26/12/21.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    var userName = "test"
    var password = "test@123"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserDefaults.standard.set("", forKey: USERNAME)
        UserDefaults.standard.set("", forKey: PASSWORD)
        
        // Do any additional setup after loading the view.
    }

    @IBAction func Login_Click(_ sender: Any) {
        
        if(txtUserName.text == "")
        {
            COMMON.showAlert(title: "Login", message: "Please Enter Username", vc: self)
        }
        else if(txtPassword.text == "")
        {
            COMMON.showAlert(title: "Login", message: "Please Enter Password", vc: self)
        }
        else if(txtUserName.text?.lowercased() == userName && txtPassword.text == password)
        {
            UserDefaults.standard.set(txtUserName.text, forKey: USERNAME)
            UserDefaults.standard.set(txtUserName.text, forKey: PASSWORD)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "menu") as? MenuViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }
        else
        {
            COMMON.showAlert(title: "Login", message: "Please Enter valid credentials", vc: self)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == txtUserName)
        {
            txtPassword.becomeFirstResponder()
        }
        else if (textField == txtPassword)
        {
            txtPassword.resignFirstResponder()
        }
        
        return true
        
    }
    
}

