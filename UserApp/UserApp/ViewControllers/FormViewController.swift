//
//  FormViewController.swift
//  UserApp
//
//  Created by Kumar, Ananda on 26/12/21.
//

import UIKit

class FormViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtEid: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtIdNo: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtUNo: UITextField!
    @IBOutlet weak var txtMobNo: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func save_Click(_ sender: Any) {
        
        if(txtEid.text == "")
        {
            COMMON.showAlert(title: "Form", message: "Emirates ID", vc: self)
        }
        else if(txtName.text == "")
        {
            COMMON.showAlert(title: "Form", message: "Please Enter Name", vc: self)
        }
        else if(txtIdNo.text == "")
        {
            COMMON.showAlert(title: "Form", message: "Please Enter Idbarah number", vc: self)
        }
        else if(txtEmail.text == "")
        {
            COMMON.showAlert(title: "Form", message: "Please Enter Email Address", vc: self)
        }
        else if(txtUNo.text == "")
        {
            COMMON.showAlert(title: "Form", message: "Please Enter Unified Number", vc: self)
        }
        else if(txtMobNo.text == "")
        {
            COMMON.showAlert(title: "Form", message: "Please Enter Mobile number", vc: self)
        }
        else if(!isValidEmailAddress(txtEmail.text ?? ""))
        {
            COMMON.showAlert(title: "Form", message: "Please Enter Valid Emaild Address", vc: self)
        }
        else if(!isValidMobileNumber(txtMobNo.text ?? ""))
        {
            COMMON.showAlert(title: "Form", message: "Please Enter Valid Mobile Number", vc: self)
        }
        else
        {
            COMMON.showProgressbar(vc: self, msg: "Post data to server...")
            perform(#selector(postFormData), with: nil, afterDelay: 0.1)
       
        }
    }
    
    @objc func postFormData()
    {
        var params:[String:Any] = [:]
        params["eid"] = txtEid.text!
        params["name"] = txtName.text!
        params["idbarahNo"] = txtIdNo.text!
        params["emailAddress"] = txtEmail.text!
        params["unifiedNumber"] = txtUNo.text!
        params["mobileNo"] = txtMobNo.text!
        
        
        APICall.performPostAPICall(parameter: params, obj: self) {(jsonResult) in
            DispatchQueue.main.async { [self] in
                if(jsonResult!.count > 0)
                {
                    COMMON.hideProgressbar()
                    if (jsonResult?.value(forKey: "success") as? Int == 1)
                    {
                        self.perform(#selector(showSuccessAlert), with: nil, afterDelay: 0.5)
                      
                    }
                    else
                    {
                        self.perform(#selector(showFailureAlert), with: nil, afterDelay: 0.5)
                    }
                }
            }
        }
    }
  
    func isValidEmailAddress(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func isValidMobileNumber(_ phone: String) -> Bool {
        let phoneRegex = "^[0-9+]{0,1}+[0-9]{5,16}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phoneTest.evaluate(with: phone)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == txtEid)
        {
            txtName.becomeFirstResponder()
        }
        else if(textField == txtName)
        {
            txtIdNo.becomeFirstResponder()
        }
        else if(textField == txtIdNo)
        {
            txtEmail.becomeFirstResponder()
        }
        else if(textField == txtEmail)
        {
            txtUNo.becomeFirstResponder()
        }
        else if(textField == txtUNo)
        {
            txtMobNo.becomeFirstResponder()
        }
        else if(textField == txtMobNo)
        {
            txtMobNo.resignFirstResponder()
        }
        
        return true
        
    }
    @objc func showSuccessAlert()
    {
        COMMON.showAlert(title: "Form", message: "Form data added successfuly", vc: self)
    }
    
    @objc func showFailureAlert()
    {
        COMMON.showAlert(title: "Form", message: "Form data added failed", vc: self)
    }
    
  

}
