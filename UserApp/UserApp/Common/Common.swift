//
//  Common.swift
//  PPC
//
//  Created by Kumar, Ananda on 27/08/21.
//

import Foundation
import UIKit

class Common
{
    static let shared = Common()
    var progressVC:ProgressbarVC?
   
    
    func showAlert(title:String, message:String, vc:UIViewController)
    {
        let valid_Alert = UIAlertController(title:title, message: message, preferredStyle: UIAlertController.Style.alert)
        valid_Alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        vc.present(valid_Alert, animated: true, completion: nil)
    }

    func showProgressbar(vc:UIViewController, msg:String)
    {
        progressVC = vc.storyboard?.instantiateViewController(withIdentifier: "ProgressbarVC") as? ProgressbarVC
        progressVC?.message = msg
        progressVC?.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        progressVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        vc.present(progressVC!, animated: true, completion: nil)
    }
    func hideProgressbar()
    {
        if(progressVC != nil)
        {
            progressVC?.dismiss(animated: true, completion: nil)
        }
    }
    
   
}
