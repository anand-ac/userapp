//
//  ProgressbarVC.swift
//  PPC
//
//  Created by Kumar, Ananda on 13/09/21.
//

import UIKit

class ProgressbarVC: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lblMessage: UILabel!
    
    var message = "Connecting to server..."
    override func viewDidLoad()
    {
        super.viewDidLoad()
              
        lblMessage.textColor = UIColor.black
        activityIndicator.color = UIColor.black
        activityIndicator.startAnimating()
        lblMessage.text = message
    }
}
