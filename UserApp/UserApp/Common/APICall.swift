//
//  APICall.swift
//  UserApp
//
//  Created by Kumar, Ananda on 26/12/21.
//

import UIKit

public enum Result<T> {
    case success(T)
    case failure(Error)
}

class APICall: NSObject {

    public class func performPostAPICall(parameter: Any, obj:UIViewController, completionHandler:@escaping(_ apiResult: NSDictionary?)->Void)
    {
        guard let url = URL(string: BASE_URL+POSTFORMURL) else {
            print("Error: cannot create URL")
            return
        }
        
        let jsonData = try? JSONSerialization.data(withJSONObject: parameter)
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue(CLIENT_KEY, forHTTPHeaderField:"consumer-key")
        request.setValue(CLIENT_SECRET, forHTTPHeaderField:"consumer-secret")
        request.httpBody = jsonData
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print("Error: error calling POST")
                print(error!)
                completionHandler(nil)
                return
            }
            guard let content = data else {
                print("Error: Did not receive data")
                completionHandler(nil)
                return
            }
            
            do {
                guard let json = (try? JSONSerialization.jsonObject(with: content, options: [])) as? NSDictionary else
                {
                    print("Error: Cannot convert JSON object to Pretty JSON data")
                    completionHandler(nil)
                    return
                }
                
                completionHandler(json)
               
            }
        }.resume()
    }
    
    public class func performGetAPICall(apiName: String, obj:UIViewController, completionHandler:@escaping(_ apiResult: NSDictionary?)->Void)
    {
        guard let url = URL(string: apiName) else {
            print("Error: cannot create URL")
            return
        }
      
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print("Error: error calling GET")
                print(error!)
                return
            }
            guard let content = data else {
                print("Error: Did not receive data")
                return
            }
            
            do {
                guard let json = (try? JSONSerialization.jsonObject(with: content, options: [])) as? NSDictionary else
                {
                    print("Error: Cannot convert JSON object to Pretty JSON data")
                    completionHandler(nil)
                    return
                }
                
                completionHandler(json)
                
                
            }
        }.resume()
    }
  
        private static func getData(url: URL,
                                    completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
            URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
        }
        
     
        public static func downloadImage(url: URL,
                                         completion: @escaping (Result<Data>) -> Void) {
            APICall.getData(url: url) { data, response, error in
                
                if let error = error {
                    completion(.failure(error))
                    return
                }
                
                guard let data = data, error == nil else {
                    return
                }
                
                DispatchQueue.main.async() {
                    completion(.success(data))
                }
            }
        }
   

    
}


